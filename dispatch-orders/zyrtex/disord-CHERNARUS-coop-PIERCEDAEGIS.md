# **DISPATCH:** `PIERCED AEGIS`
## **Terrain:** `CHERNARUS`

<a href="https://postimg.cc/image/tolh31rsb/" target="_blank"><img src="https://s9.postimg.cc/tolh31rsb/20180519225201_1.jpg" alt="20180519225201_1"/></a><br/><br/>


### **Situation**

`A Company is paying us top dollars to keep the area around one of their compound safe in an unsafe area`

**BLUFOR**

`Wolfpack and Hammer`

<a href="https://postimg.cc/image/lvutb56ej/" target="_blank"><img src="https://s9.postimg.cc/lvutb56ej/20180519224303_1.jpg" alt="20180519224303_1"/></a><br/><br/>`

**REDFOR**

`Chernarus Red Star Movement`

<a href="https://postimg.cc/image/ki36fbza3/" target="_blank"><img src="https://s9.postimg.cc/ki36fbza3/20180519223919_1.jpg" alt="20180519223919_1"/></a><br/><br/>

**GREENFOR**

`Rival Private Security Company`

<a href="https://postimg.cc/image/826gm23ij/" target="_blank"><img src="https://s9.postimg.cc/826gm23ij/20180519224126_1.jpg" alt="20180519224126_1"/></a><br/><br/>

### **Mission**

- `Patrol the area around the compound using the assets provided`
- `Keep the area secured`
- `Prevent the destruction of any company asset`

### **Execution**

**Commander's Intent**

- `The client is paying us to keep the compound in working order. REMEMBER THAT`
- `A rival company is being contracted at the same time. Make sure you do better so only us get the contract `
- `Just try to be somewhat professional. We need that money`

**Movement Plan**

`Patrol path will be provided by the head of security of the compound.`

### **Extra Information**

- `The rival security company may try something. Be wary of those a-holes`
- `We don't know what our client do here, we don't care but the locals seems to care. We're in for the money. If they come close you shoot !`
