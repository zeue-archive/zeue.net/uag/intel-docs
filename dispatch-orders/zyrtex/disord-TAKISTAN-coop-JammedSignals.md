# **DISPATCH:** `JAMMED SIGNALS`
## **Terrain:** `TAKISTAN`

<a href='https://postimg.cc/image/j4rfs65ij/' target='_blank'><img src='https://s9.postimg.cc/j4rfs65ij/xp6917w24fjy.jpg' border='0' alt='xp6917w24fjy'/></a>


### **Situation**

`We have been tasked to assist with destabilizing the country by detroying key communication arrays.`

**BLUFOR**

`Ghost Team (CIA operatives)`

<a href="https://postimg.cc/image/w4vktcnjv/" target="_blank"><img src="https://s17.postimg.cc/w4vktcnjv/20180501175033_1.jpg" alt="20180501175033_1"/></a><br/><br/>

**REDFOR**

`Takistan Government Forces`

<a href="https://postimg.cc/image/6yummitzf/" target="_blank"><img src="https://s17.postimg.cc/6yummitzf/20180501173535_1.jpg" alt="20180501173535_1"/></a><br/><br/>

**GREENFOR**

`Local Militias`

<a href="https://postimg.cc/image/czsbjktgb/" target="_blank"><img src="https://s17.postimg.cc/czsbjktgb/20180501173149_1.jpg" alt="20180501173149_1"/></a><br/><br/>

### **Mission**

- `Locate communications arrays`
- `Blow up the arrays`
- `Extract from the area`

### **Execution**

**Commander's Intent**

- `Make sure CIA's involvment is not discovered`
- `Leave no man behind`
- `The objective is to disrupt not total annihilation`

**Movement Plan**

`Mobility is paramount. Air assets will not be provided. Only local ground vehicles will be available.`

### **Extra Information**

- `Ghost team will be able to provide drone coverage but they are reluctant to expose themselves. Expect little cooperation.`
- `Local militias could be convinced to provide extra assets and/or help. However don't expect too much from them as they are well under-equipped.`
