# **DISPATCH:** `CRACKED EGGS`
## **Terrain:** `CHERNARUS`

<a href="https://postimg.cc/image/tolh31rsb/" target="_blank"><img src="https://s9.postimg.cc/tolh31rsb/20180519225201_1.jpg" alt="20180519225201_1"/></a><br/><br/>

### **Situation**

`We have been tasked to assist with destabilizing the country by detroying key communication arrays.`

**BLUFOR**

`Wolfpack`

**REDFOR**

`Russian Armed Forces`

<a href="https://postimg.cc/image/e345j5kzv/" target="_blank"><img src="https://s9.postimg.cc/e345j5kzv/20180519224616_1.jpg" alt="20180519224616_1"/></a><br/><br/>

**GREENFOR**

`Local Militias`

<a href="https://postimg.cc/image/ic8vladyj/" target="_blank"><img src="https://s9.postimg.cc/ic8vladyj/20180519225145_1.jpg" alt="20180519225145_1"/></a><br/><br/>

### **Mission**

- `Get to the research site`
- `Capture at least 1 (one) scientist.`
- `Extract with the captive and any intel you may come across`

### **Execution**

**Commander's Intent**

- `Get in quick, grab what you can, destroy what you can't`
- `Leave no man behind`
- `You'll be overrun if you stay. As soon as you complete the objective GET THE HELL OUT!`

**Movement Plan**

`Try to get as close as possible without warning the enemy.`

### **Extra Information**

- `Local militias could be used to draw some fire away from you. They are not considered a threat to the russians and they are too disorganised to manage anything significant.`
