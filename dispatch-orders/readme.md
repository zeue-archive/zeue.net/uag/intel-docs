# Dispatch Orders (DISORDs)

Dispatch orders are reusable and generic missions that the unit can play whenever there isn't a [dispatch order](https://gitlab.com/armapmc/uag/ncg-intel/intel-docs-backlog/tree/master/operation-orders) available.

Please read over the [disord-template](https://gitlab.com/armapmc/uag/ncg-intel/intel-docs-backlog/blob/master/dispatch-orders/disord-TERRAIN-TYPE-examplefile.md) before continuing.