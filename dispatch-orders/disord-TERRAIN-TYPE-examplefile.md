> This is an example file for UAG Dispatch Orders, please try and follow the formatting. 
>
> The file name should be `disord-TERRAIN-TYPE-[title].md` without the brackets.
>
> You can read more about how to fomat this file [here](https://docs.gitlab.com/ee/user/markdown.html).
>
> Only edit things that are formatted `like this`
>
> For images we recommend using [this site](https://postimages.org/) and using their **thumbnail for website** option.

# **DISPATCH:** `Example Name`
## **Terrain:** `Lingor`

<a href='https://postimg.cc/image/uqxrobh17/' target='_blank'><img src='https://s9.postimg.cc/uqxrobh17/lingor.jpg' border='0' alt='lingor'/></a>


### **Situation**

`This is an example discord situation section. The someone of somewhere is doing bad stuff and we have been sent to stop them.`

**BLUFOR**

`These guys will help us`

<a href="https://postimg.cc/image/7vuvlm0sb/" target="_blank"><img src="https://s9.postimg.cc/7vuvlm0sb/rhsusaf-version-042-1_4.jpg" alt="rhsusaf-version-042-1_4"/></a><br/><br/>

**REDFOR**

`These guys will hurt us`

<a href="https://postimg.cc/image/m2amgu3xn/" target="_blank"><img src="https://s9.postimg.cc/m2amgu3xn/maxresdefault.jpg" alt="maxresdefault"/></a><br/><br/>

**GREENFOR**

`(OPTIONAL) Other guys that may help us or kill us idk who do you think I am???`

<a href="https://postimg.cc/image/wp4fm94d7/" target="_blank"><img src="https://s9.postimg.cc/wp4fm94d7/20160529192148_1b7z4w.jpg" alt="20160529192148_1b7z4w"/></a><br/><br/>

### **Mission**

`This is an example discord mission section, here you will briefly detail any objectives the unit has in this operation:`

- `Move to somewhere`
- `Kill someone`
- `Secure a place`

### **Execution**

`This is an example discord execution section, here you will list important points regarding mission parameters:`

**Commander's Intent**

`Complete the mission without a faction finding out`

**Movement Plan**

`Use some sort of deployment method, like boat or paradrop`

### **Extra Information**

`This is where you put other cool stuff, such as intelligence pictures or map data UwU`
