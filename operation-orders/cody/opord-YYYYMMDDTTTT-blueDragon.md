# **OPERATION:** `Blue Dragon`
## **Deployment ID: YYYYMMDDTTTT**
## **Terrain:** `Lingor`

<a href='https://postimg.cc/image/uqxrobh17/' target='_blank'><img src='https://s9.postimg.cc/uqxrobh17/lingor.jpg' border='0' alt='lingor'/></a>

### **Situation**

`Before we were UAG, most of us worked for a contracting company called PRG. The fall of that group came in April 2018, at the hands of a NATO-PLA task force dubbed the "Blue Dragon JTF," or BDJTF for short. We have recently gained some intel about their whereabouts and need to get it verified before we strike, so HQ has tasked Wolfpack and Badger to infiltrate a known PLA defense station that housed BDJTF wounded after a recent engagement.`

**BLUFOR**

`Wolfpack, Badger, and a V-44 Valkyrie `

<a href='https://postimg.cc/image/czm59n44r/' target='_blank'><img src='https://s9.postimg.cc/czm59n44r/v44.jpg' border='0' alt='v44'/></a>

**REDFOR**

`PLA Special Defense Force`

<a href='https://postimg.cc/image/ca3cxbdvv/' target='_blank'><img src='https://s9.postimg.cc/ca3cxbdvv/pla.jpg' border='0' alt='pla'/></a>

**GREENFOR**

`None`

### **Mission**

`Our job is simple, we are to move in and retrieve any information we can regarding the BDJTF and their current operations. Here's the plan:`

- `Insert into the AO via HALO drop and regroup forces once on the ground`
- `Move to the site of the defense station and infiltrate it's security`
- `Secure evidence regarding the BDJTF's whereabouts and their operations`
- `Destroy any facilities or equipment that may aid the BDJTF inside the defense station`

### **Execution**

`Stealth is our number-one priority for this operation, if the BDJTF finds out about this then we will surely find it much more difficult to track them down.`

**Commander's Intent**

`Secure intel on the BDJTF without alerting them or the PLA to our existence`

**Movement Plan**

`HALO drop from hijacked civilian aviation, ground-movement restricted to night`

### **Extra Information**

`None`
