# Operation Orders (OPORDs)

Operation orders are special one-off missions that are heavily story driven and usually require a much larger turnout than [dispatch orders](https://gitlab.com/armapmc/uag/ncg-intel/intel-docs-backlog/tree/master/dispatch-orders).

Please read over the [opord-template](https://gitlab.com/armapmc/uag/ncg-intel/intel-docs-backlog/blob/master/operation-orders/opord-YYYYMMDDTTTT-examplefile.md) before continuing.