# **DISPATCH:** `Sandy Guardians`
## **Terrain:** `Takistan`

<a href='https://postimg.cc/image/j4rfs65ij/' target='_blank'><img src='https://s9.postimg.cc/j4rfs65ij/xp6917w24fjy.jpg' border='0' alt='xp6917w24fjy'/></a>

### **Situation**

`We have been tasked with safely moving valuable supplies around the region of Takistan.`

**BLUFOR**

`Wolfpack and Reaper`

<a href='https://postimg.cc/image/evmpq9a1n/' target='_blank'><img src='https://s9.postimg.cc/evmpq9a1n/621-hellfireii_dagr-missle_rrp.jpg' border='0' alt='621-hellfireii_dagr-missle_rrp'/></a>

**REDFOR**

`Islamic State of Takistan`

<a href='https://postimg.cc/image/9wz7bsqu3/' target='_blank'><img src='https://s9.postimg.cc/9wz7bsqu3/2014_11_05_00004_4.jpg' border='0' alt='2014_11_05_00004_4'/></a>

**GREENFOR**

`None`

### **Mission**

- `Move to convoy starting point and mount supplies`
- `Safely deliver the supplies to the target destination`
- `Repeat until all routes are completed`

### **Execution**

**Commander's Intent**

`Safely secure and deliver all supplies without losing more than 30% of the cargo`

**Movement Plan**

`Use of mechanized vehicles is permitted, but speed is key with this mission`

### **Extra Information**

`None`
