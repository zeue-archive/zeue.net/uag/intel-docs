Hello Collin, this is how you contribute to this repo:

1. Think of a really cool mission
2. Create a [new branch](https://docs.gitlab.com/ee/gitlab-basics/create-branch.html) from the `master` origin
3. Copy the source code of the [template discord](https://gitlab.com/armapmc/uag/ncg-intel/intel-docs-backlog/blob/master/dispatch-orders/disord-DRAFT-examplefile.md) and fill it out with what your idea is.
4. Create a [merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) and target upstream (make the target `master`)
5. Submit it and wait! Cody will come along and merge it himself, please avoid merging it yourself until he has greenlit it.

Once your submission has been selected and assigned to a session, your proposal will be deleted from the backlog repository and archived on NCG:Intel's[release repo](https://gitlab.com/armapmc/uag/ncg-intel/intel-docs-public).